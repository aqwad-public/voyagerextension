var temp;


$(".btn_create").on('click',(i)=>{
  $("#createId").text($(i.target).data("create"));
    $.ajax({
        url: '/admin/'+$(i.target).data("create")+'/create',
        type: 'GET',
        success: function(data){
          $('#modal1').html($(data));
        }
      })
});

    $('.btn_edit').on('click',(i)=>{
      $("#editId").text($(i.target).data("edit"));
        temp = $(i.target).data("slug"); 

       $.ajax({
        url: '/admin/'+temp+'/'+$(i.target).data("edit")+'/edit',
        type: 'GET',
        success: function(data){
          $('#modal2').html($(data));
        }
      })
      
    });


    $('.btn_delete').on('click',(i)=>{
      $("#myID").val($(i.target).data("delete"));
      temp = $(i.target).data("slug"); 

    });

$('#del-btn').on('click',function(e){
      e.preventDefault();

      var deleted = $("#myID").val();
       $.ajax({
        url: '/admin/'+temp+'/'+deleted,
        type: 'DELETE',
        success: function(){
          location.reload();
        },
        error: function(xhr, desc, err) {
          location.reload();
        },
      })
      
    });
