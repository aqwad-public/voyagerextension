<?php

namespace Akwad\VoyagerExtension;

use Illuminate\Support\ServiceProvider;
//use Akwad\VoyagerExtension\VoyagerExtension;

class VoyagerExtensionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'VoyagerExtension');
        // $this->app->singleton('voyager', function () {
        //     return new VoyagerExtension();
        // });


    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadHelpers();
    }

    protected function loadHelpers()
    {
        foreach (glob(__DIR__.'/Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }

  
}
