<?php

namespace Akwad\VoyagerExtension\Http\Controllers\formfields;

use Carbon\Carbon;

class TimestampHandler extends AbstractHandler
{
    protected $codename = 'timestamp';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.timestamp', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }

    public static function getContent($request,$slug,$row){
          if (!in_array($this->request->method(), ['PUT', 'POST'])) {
            return;
        }

        $content = $this->request->input($this->row->field);

        if (empty($content)) {
            return;
        }

        return Carbon::parse($content);
    }
}
