<?php

namespace Akwad\VoyagerExtension\Http\Controllers\formfields;

class CoordinatesHandler extends AbstractHandler
{
    protected $supports = [
        'mysql',
        'pgsql',
    ];

    protected $codename = 'coordinates';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.coordinates', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }

    public function getContent($request,$slug,$row)
    {
        if (empty($coordinates = $request->input($row->field))) {
            return;
        }
        //DB::connection()->getPdo()->quote won't work as it quotes the
        // lat/lng, which leads to wrong Geometry type in POINT() MySQL constructor
        $lat = (float) $coordinates['lat'];
        $lng = (float) $coordinates['lng'];

        return DB::raw("ST_GeomFromText('POINT({$lng} {$lat})')");
    }
}
