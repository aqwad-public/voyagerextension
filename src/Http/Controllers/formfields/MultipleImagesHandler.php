<?php

namespace Akwad\VoyagerExtension\Http\Controllers\formfields;

class MultipleImagesHandler extends AbstractHandler
{
    protected $codename = 'MultipleImages';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.multiple_images', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
