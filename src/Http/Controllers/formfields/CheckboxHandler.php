<?php

namespace Akwad\VoyagerExtension\Http\Controllers\formfields;

class CheckboxHandler extends AbstractHandler
{
    protected $codename = 'checkbox';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.checkbox', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
    public static function getContent($request, $slug,$row){
        return (int) ($request->input($row->field) == 'on');
    }
}
