<?php

namespace Akwad\VoyagerExtension\Http\Controllers\formfields;

use TCG\Voyager\FormFields\AbstractHandler;

class CalendarHandler extends AbstractHandler
{
    protected $codename = 'calendar';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('calendar', [
            'row' => $row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent
        ]);
    }
    public function getContent(){
        
    }
}