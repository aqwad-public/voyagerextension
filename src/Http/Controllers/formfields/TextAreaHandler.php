<?php

namespace Akwad\VoyagerExtension\Http\Controllers\formfields;

class TextAreaHandler extends AbstractHandler
{
    protected $codename = 'TextArea';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.text_area', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
