<?php

namespace Akwad\VoyagerExtension\Http\Controllers\formfields;

class RadioBtnHandler extends AbstractHandler
{
    protected $name = 'Radio Button';
    protected $codename = 'RadioBtn';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.radio_btn', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
