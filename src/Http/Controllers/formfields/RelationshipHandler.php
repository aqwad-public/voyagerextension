<?php

namespace Akwad\VoyagerExtension\Http\Controllers\formfields;

class Relationship extends BaseType
{
    /**
     * @return array
     */
    public function getContent($request, $slug, $row)
    {
        $content = $this->request->input($this->row->field);
        if (is_array($content)) {
            for ($i = 0; $i < count($content); $i++) {
                if ($content[$i] === null) {
                    unset($content[$i]);
                }
            }
        }

        return $content;
    }
}
