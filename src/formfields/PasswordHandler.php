<?php

namespace Akwad\VoyagerExtension\formfields;

class PasswordHandler extends AbstractHandler
{
    protected $codename = 'password';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.password', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
     public function getContent($request, $slug, $row)
    {
        return empty($request->input($row->field)) ? null :
            bcrypt($request->input($row->field));
    }
}
