<?php

namespace Akwad\VoyagerExtension\formfields;

class NumberHandler extends AbstractHandler
{
    protected $codename = 'number';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.number', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
    public function getContent($request, $slug, $row){
        $value = $request->input($row->field);

        if (isset($this->options->null)) {
            return $value == $this->options->null ? null : $value;
        }

        return $value; 
    }
}
