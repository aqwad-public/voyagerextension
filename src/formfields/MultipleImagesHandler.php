<?php

namespace Akwad\VoyagerExtension\formfields;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image as InterventionImage;

class MultipleImagesHandler extends AbstractHandler
{
    protected $codename = 'MultipleImages';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.multiple_images', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
    public function getContent($request, $row, $slug){
        $filesPath = [];
        $files = $request->file($row->field);

        if (!$files) {
            return;
        }

        if(!is_array($files)){
            $files = [$files];
        }

        foreach ($files as $file) {
            $image = InterventionImage::make($file);

            $resize_width = null;
            $resize_height = null;

           
            $resize_width = $image->width();
            $resize_height = $image->height();
            

            $resize_quality =  75;

            $filename = Str::random(20);
            $path = $slug.DIRECTORY_SEPARATOR.date('FY').DIRECTORY_SEPARATOR;
            array_push($filesPath, asset('/storage/'.$path.$filename.'.'.$file->getClientOriginalExtension()));
            $filePath = $path.$filename.'.'.$file->getClientOriginalExtension();

            $image = $image->resize(
                $resize_width,
                $resize_height,
                function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    
                }
            )->encode($file->getClientOriginalExtension(), $resize_quality);

            Storage::disk(config('voyager.storage.disk'))->put($filePath, (string) $image, 'public');

            
        }

        return $filesPath;
    }


    }

