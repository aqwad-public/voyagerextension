<?php

namespace Akwad\VoyagerExtension\formfields;

use TCG\Voyager\FormFields\AbstractHandler;
use Carbon\Carbon;

class CalendarHandler extends AbstractHandler
{
    protected $codename = 'calendar';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('calendar', [
            'row' => $row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent
        ]);
    }
    public function getContent($request, $slug, $row){
        $mealdays = explode(',', $request->day);
        $days = "";
        foreach ($mealdays as $day) {
            $day = substr($day, 0, 33);
            $carbon = new Carbon($day);
            $carbon = $carbon->toDateString();
            $days .= $carbon . ",";
        }
        return $days;
    }
}