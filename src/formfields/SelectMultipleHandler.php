<?php

namespace Akwad\VoyagerExtension\formfields;

class SelectMultipleHandler extends AbstractHandler
{
    protected $codename = 'select_multiple';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.select_multiple', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }

    public  function getContent($request, $slug, $row){
         $content = $request->input($row->field, []);

        if (true === empty($content)) {
            return json_encode([]);
        }

        // Check if we need to parse the editablePivotFields to update fields in the corresponding pivot table

        if (isset($this->options->relationship) && !empty($this->options->relationship->editablePivotFields)) {
            $pivotContent = [];
            // Read all values for fields in pivot tables from the request
            foreach ($this->options->relationship->editablePivotFields as $pivotField) {
                if (!isset($pivotContent[$pivotField])) {
                    $pivotContent[$pivotField] = [];
                }
                $pivotContent[$pivotField] = $this->request->input('pivot_'.$pivotField);
            }
            // Create a new content array for updating pivot table
            $newContent = [];
            foreach ($content as $contentIndex => $contentValue) {
                $newContent[$contentValue] = [];
                foreach ($pivotContent as $pivotContentKey => $value) {
                    $newContent[$contentValue][$pivotContentKey] = $value[$contentIndex];
                }
            }
            $content = $newContent;
        }

        return json_encode($content);
    }
}
