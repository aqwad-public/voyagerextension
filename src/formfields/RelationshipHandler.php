<?php

namespace Akwad\VoyagerExtension\formfields;

class RelationshipHandler extends AbstractHandler
{
    protected $codename = 'relationship';
    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.relationship', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }

    /**
     * @return array
     */
    public function getContent($request, $slug, $row)
    {
        $content = $request->input($row->field);
        if (is_array($content)) {
            for ($i = 0; $i < count($content); $i++) {
                if ($content[$i] === null) {
                    unset($content[$i]);
                }
            }
        }

        return $content;
    }
}
