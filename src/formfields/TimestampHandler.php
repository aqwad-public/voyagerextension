<?php

namespace Akwad\VoyagerExtension\formfields;

use Carbon\Carbon;

class TimestampHandler extends AbstractHandler
{
    protected $codename = 'timestamp';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.timestamp', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }

    public  function getContent($request,$slug,$row){
          if (!in_array($request->method(), ['PUT', 'POST'])) {
            return;
        }

        $content = $request->input($row->field);

        if (empty($content)) {
            return;
        }

        return Carbon::parse($content);
    }
}
