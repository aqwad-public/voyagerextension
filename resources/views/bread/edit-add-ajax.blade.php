@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());


   
@endphp
@extends( 'VoyagerExtension::master-ajax' )
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))



@section('content')
    <div class="page-content edit-add">
        <div class="row">
            <div class="col-md-12" style="margin-bottom:0">

                <div id="loading-container " style="display:none" class="ajax-loader">
                    <div id="voyager-loader" style=" position:relative;min-height:200px">
                        <?php $admin_loader_img = Voyager::setting('admin.loader', ''); ?>
                        @if($admin_loader_img == '')
                            <img style="top: calc(50% - 50px);" src="{{ voyager_asset('images/logo-icon.png') }}" alt="Voyager Loader">
                        @else
                            <img style="top: calc(50% - 50px);" src="{{ Voyager::image($admin_loader_img) }}" alt="Voyager Loader">
                        @endif
                    </div>
                </div>

                <!-- Our mini PopUp Modal -->
                <div class="modal modal-primary fade" tabindex="-1" id="{{$dataType->slug}}-create-edit-modal" role="dialog" data-keyboard="false" data-backdrop="static">
                    <div style="margin-top:53px; margin-left:-16px" class="modal-lg">
                        <div class="modal-header">
                            <span id="{{$dataType->slug}}-title">Details</span>
                            <button id="{{$dataType->slug}}-close_button" name="" type="button"  class="close">&times;</button>
                        </div>
                        <div class="modal-body" name="" style="padding:0" id="{{$dataType->slug}}-create-edit-modal-body">
                        </div>
                    </div>
                    <div style="background:rgba(0,0,0,0.5); height:100%; padding:0; margin:-30px;"></div>
                </div>
                <!-- Our mini PopUp Modal -->

                <div class="panel panel-bordered" id="ajax-panel-borderd" style="display:block;margin-bottom:0">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add formajax"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp
                            @foreach($dataTypeRows as $row)
                                @if($row->type == "relationship" && strpos($row->field, 'hasmany') !== false)
                                    <script>
                                        family_tree["{{str_replace('_', '-',$row->details->table)}}"] = {
                                            parent_slug: "{{$dataType->slug}}",
                                            parent: "{{$dataType->name}}",
                                            parentId: "{{$dataTypeContent->id}}",
                                            model_name: "{{str_replace('_', '-',$row->details->table)}}",
                                            ajax: true
                                        };
                                    </script>
                                @endif
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif
                                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label" for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if($row->readonly)
                                        <input readonly="readonly" @if($row->required == 1) required @endif type="text" class="form-control" name="{{ $row->field }}"
                                                placeholder="{{ old($row->field ?? $row->getTranslatedAttribute('display_name')) }}"
                                                value="{{ old($row->field, $dataTypeContent->{$row->field} ?? '') }}">
                                    @elseif (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add')])
                                    @elseif ($row->type == 'relationship')
                                       
                                        @include('VoyagerExtension::formfields.relationship-ajax', ['options' => $row->details,'parent'=>$parent,'parentId'=>$parentId])


                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!} 
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)

                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!} 
                                    @endforeach
                                    @if ($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>
                  
                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>
    
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;
        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });
            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();


        });
    </script>

    <script>                                                                                                                            
        $(".formajax").on('submit',function(event){
            event.preventDefault();
            let fromData= new FormData(event.target);
            $(".ajax-loader").show();
            $("#ajax-panel-borderd").hide();
            $.ajax({
                url: $(event.target).attr('action')+"?ajax",
                type: 'POST',
                data: fromData,
                contentType:false,
                processData:false,
                success: function() {
                    event.preventDefault();
                    $(event.target).closest('.modal-body').html('');
                    location.reload();
                },
                error: function(xhr, desc, err) {
                    let error_html = '<div style ="padding-top:15px;color:red;background-color:white;min-height:150px;"><h4 style="margin-left:5px;color:black;">Sorry, some fields are invalid:</h4><ul>';
                    if(xhr){
                        let error_fields = xhr.responseJSON.errors;
                        for (let [key, value] of Object.entries(error_fields)) {
                            error_html+=`<li> <span style="font-weight:bold">${key}</span> : ${value} </li>`;
                        }
                        error_html+="</ul></div>";
                    } 
                    // error_fields.each(function(index, value) {
                    // console.log(`${index}: ${value}`);
                    // });
                    //console.log("Details0: " + desc + "\nError:" + err);
                    $("#ajax-panel-borderd").show();
                    $(".ajax-loader").hide();
                    $(event.target).closest('.modal-body').html(error_html);
                    
                    return true;
                },
            })
            return false;
        });                                                                       
    </script>
    <script type="text/javascript" src="{{ asset('js/ajax_app.js') }}"></script>
@stop
