

@if(isset($options->model) && isset($options->type))

    @if(class_exists($options->model))

        @php 
        $relationshipField = $row->field;
        $newName = $parent;

        if(strpos($parent,'ies')){
            $newName = substr($newName,0,-3)."y";
        }
        if(strpos($parent,'es')){
            $newName = substr($newName,0,-2)."";
        }
        @endphp

        @if(strpos($row->field,$newName)!==false||strpos($row->field,substr($newName,0,-1))!==false)
        
            @php 
            $model = app($options->model);
            $selectedModel = $model::where($options->key,$parentId)->first();
            @endphp
                    <select  class="form-control" name="{{ $options->column }}" style="visibility:hidden;height:0" >
                        <option selected="selected" value="{{$parentId}}">{{$selectedModel->{$options->label} }}</option>
                    </select>
                    <input class="form-control" readonly="readonly" value="{{$selectedModel->{$options->label} }}">
 

 

        @elseif($options->type == 'belongsTo')

            @if(isset($view) && ($view == 'browse' || $view == 'read'))

                @php
                    $relationshipData = (isset($data)) ? $data : $dataTypeContent;
                    $model = app($options->model);
                    $query = $model::where($options->key,$relationshipData->{$options->column})->first();
                @endphp

                @if(isset($query))
                    <p>{{ $query->{$options->label} }}</p>
                @else
                    <p>{{ __('voyager::generic.no_results') }}</p>
                @endif

            @else
               
                <select
                    class="form-control select2-ajax" name="{{ $options->column }}"
                    data-get-items-route="{{route('voyager.' . $dataType->slug.'.relation')}}"
                    data-get-items-field="{{$row->field}}"
                    data-method="{{ isset($dataTypeContent) ? 'edit' : 'add' }}"
                >
                    @php
                        $model = app($options->model);
                        $query = $model::where($options->key, $dataTypeContent->{$options->column})->get();
                    @endphp

                    @if(!$row->required)
                        <option value="">{{__('voyager::generic.none')}}</option>
                    @endif

                    @foreach($query as $relationshipData)
                        <option value="{{ $relationshipData->{$options->key} }}" @if($dataTypeContent->{$options->column} == $relationshipData->{$options->key}){{ 'selected="selected"' }}@endif>{{ $relationshipData->{$options->label} }}</option>
                    @endforeach
                </select>

            @endif

        @elseif($options->type == 'hasOne')

            @php

                $relationshipData = (isset($data)) ? $data : $dataTypeContent;

                $model = app($options->model);
                $query = $model::where($options->column, '=', $relationshipData->id)->first();

            @endphp

            @if(isset($query))
                <p>{{ $query->{$options->label} }}</p>
            @else
                <p>{{ __('voyager::generic.no_results') }}</p>
            @endif
<!-- //////////////////////////////////////////////////////////////////////////////////////////////////// -->
        @elseif($options->type == 'hasMany')

       
        
            @if(isset($view) && ($view == 'browse' || $view == 'read'))
                @php
                    $relationshipData = (isset($data)) ? $data : $dataTypeContent;
                    $model = app($options->model);
                    $selected_values = $model::where($options->column, '=', $relationshipData->id)->get()->map(function ($item, $key) use ($options) {
                        return $item->{$options->label};
                    })->all();
                @endphp
                @if($view == 'browse')
                    @php
                        $string_values = implode(", ", $selected_values);
                        if(mb_strlen($string_values) > 25){ $string_values = mb_substr($string_values, 0, 25) . '...'; }
                    @endphp
                    @if(empty($selected_values))
                        <p>{{ __('voyager::generic.no_results') }}</p>
                    @else
                        <p>{{ $string_values }}</p>
                    @endif
                @else
                    @if(empty($selected_values))
                        <p>{{ __('voyager::generic.no_results') }}</p>
                    @else
                        <ul>
                            @foreach($selected_values as $selected_value)
                                <li>{{ $selected_value }}</li>
                            @endforeach
                        </ul>
                    @endif
                @endif
                
            @else
                @php
                    $model = app($options->model);

                    $query = $model::where($options->column, '=', $dataTypeContent->getKey())->get();
                @endphp
                @if(empty($query))
                    <p>{{ __('voyager::generic.no_results') }}</p>
                @else


                @if(Auth::user()->can('add', app($row->details->model)))
                    <button data-create={!!$row->details->table!!} class="btn btn-primary btn_create" type="button" data-toggle="modal" data-target="#{{$dataType->slug}}-create-edit-modal">Add {!!$row->display_name!!}</button>
                @endif

                <!-- <div class="modal modal-primary fade" tabindex="-1" id="myModal" role="dialog">\
                    <div class="modal-dialog modal-lg ">
                        <div class="modal-header">
                            Add a {{$options->label}}
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="padding:0" id="modal1">
                            
                        </div>
                    </div>
                </div> -->
                
                <!-- <div class="modal fade" id="myModal2" role="dialog" style="background-color: #22A7F0; color: white;"> -->

                <!-- <div class="modal-header" style="background:white;"> -->
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                    <div class="modal-body" id="modal2">
                        
                    </div>
                </div> -->
                    @if(isset($query))
                    <div class="table-responsive">
                    <table id="dataTable" class="table table-hover">
                        <thead>
                            <tr>
                                @php
                                if(count($query) != 0){
                                $heads = array_keys($query[0]->getAttributes());
                                for($i=0; $i < count($heads); $i++)
                                if(($heads[$i]=='created_at') || ($heads[$i]=='updated_at') || ($heads[$i]=='id')){
                                    unset($heads[$i]);
                                }

                                $num_of_rows = count($query);

                                
                                @endphp

                                @foreach($heads as $head)
                                <th>
                                    {{ $head }}
                                </th>
                                @endforeach
                                <th class="actions">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $canEdit = Auth::user()->can('edit', app($row->details->model));
                            $canDelete = Auth::user()->can('delete', app($row->details->model));
                                foreach($query as $query_res){
                                $tmp = $query_res->getAttributes();
                            @endphp
                                <tr>
                                @foreach($heads as $head)

                                    <td>
                                            {{ $tmp[$head] }}
                                    </td>
                                @endforeach
                                    <td>
                                        @if($canEdit)
                                            <button type="button" data-edit="{{$query_res->getAttributes()['id']}}" data-slug="{{$query_res->getTable()}}" class="btn_edit btn btn-primary" data-toggle="modal" data-target="#{{$dataType->slug}}-create-edit-modal">{{ __('voyager::generic.edit') }}</button>
                                        @endif
                                        @if($canDelete)
                                            <button type="button" data-delete="{{$query_res->getAttributes()['id']}}" data-slug="{{$query_res->getTable()}}" class="btn_delete btn btn-danger" data-toggle="modal" data-target="#delete_modal">{{ __('voyager::generic.delete') }}</button>  
                                        @endif
                                    </td>
                                </tr>
                                @php
                            }
                        }
                            @endphp
                        </tbody>
                    </table>
                    </div>
                @endif

            @endif
        @endif
<!--///////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        @elseif($options->type == 'belongsToMany')

            @if(isset($view) && ($view == 'browse' || $view == 'read'))

                @php
                    $relationshipData = (isset($data)) ? $data : $dataTypeContent;

                    $selected_values = isset($relationshipData) ? $relationshipData->belongsToMany($options->model, $options->pivot_table)->get()->map(function ($item, $key) use ($options) {
                        return $item->{$options->label};
                    })->all() : array();
                @endphp

                @if($view == 'browse')
                    @php
                        $string_values = implode(", ", $selected_values);
                        if(mb_strlen($string_values) > 25){ $string_values = mb_substr($string_values, 0, 25) . '...'; }
                    @endphp
                    @if(empty($selected_values))
                        <p>{{ __('voyager::generic.no_results') }}</p>
                    @else
                        <p>{{ $string_values }}</p>
                    @endif
                @else
                    @if(empty($selected_values))
                        <p>{{ __('voyager::generic.no_results') }}</p>
                    @else
                        <ul>
                            @foreach($selected_values as $selected_value)
                                <li>{{ $selected_value }}</li>
                            @endforeach
                        </ul>
                    @endif
                @endif

            @else
                <select
                    class="form-control @if(isset($options->taggable) && $options->taggable == 'on') select2-taggable @else select2-ajax @endif" 
                    name="{{ $relationshipField }}[]" multiple
                    data-get-items-route="{{route('voyager.' . $dataType->slug.'.relation')}}"
                    data-get-items-field="{{$row->field}}"
                    @if(isset($options->taggable) && $options->taggable == 'on')
                        data-route="{{ route('voyager.'.\Illuminate\Support\Str::slug($options->table).'.store') }}"
                        data-label="{{$options->label}}"
                        data-error-message="{{__('voyager::bread.error_tagging')}}"
                    @endif
                >

                        @php
                            $selected_values = isset($dataTypeContent) ? $dataTypeContent->belongsToMany($options->model, $options->pivot_table)->get()->map(function ($item, $key) use ($options) {
                                return $item->{$options->key};
                            })->all() : array();
                            $relationshipOptions = app($options->model)->all();
                        @endphp

                        @if(!$row->required)
                            <option value="">{{__('voyager::generic.none')}}</option>
                        @endif

                        @foreach($relationshipOptions as $relationshipOption)
                            <option value="{{ $relationshipOption->{$options->key} }}" @if(in_array($relationshipOption->{$options->key}, $selected_values)){{ 'selected="selected"' }}@endif>{{ $relationshipOption->{$options->label} }}</option>
                        @endforeach

                </select>      

            @endif

        @endif

    @else

        cannot make relationship because {{ $options->model }} does not exist.

    @endif

@endif


