<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
</head>

<body>
	<div class="container">
		
		<div class="panel panel-primary">
			<div class="panel-body">
					{{!! $calender_details->calendar() !!}}

			</div>

		</div>

	</div>

	<script src="{{asset('public/js/app.js')}}"></script>
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>


</body>